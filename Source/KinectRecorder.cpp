#include "OpenCVKinectDevice.hpp"
#include <chrono>
#include <memory>
#include <iostream>

// Definition of the time duration of one frame, this is a double precision 1/30th of second interval
using frameDuration = std::chrono::duration<double, std::ratio<1, 30>>;

int main(int argc, char **argv)
{
    std::cout << "Video will be recorded as depth.avi" << std::endl;
    std::cout << "Press r to start recording, press q to quit" << std::endl;

    // Make the opencv kinect device
    Freenect::Freenect freenect;
    std::unique_ptr<OpenCVKinectDevice> device(&freenect.createDevice<OpenCVKinectDevice>(0));

    // Start video and depth recording (probably dont need video e.g. color stream)
    device->startVideo();
    device->startDepth();

    // Grab a probe image to get frame width and height (could hard code it but this is safer)
    cv::Mat probe;
    device->GrayDepth(probe);

    // Create the videowriter for depth.avi
    int fourcc = CV_FOURCC('D', 'I', 'V', 'X'); // DIVX fourcc -> mpeg4 codec
    cv::VideoWriter output("depth.avi", fourcc, 30.0, probe.size(), false);

    // Start the recording loop
    bool record = false;
    char press = '\0';
    while(press != 'q')
    {
        // Grab the latest kinect frame
        cv::Mat frame;
        device->GrayDepth(frame);

        // display and write the frame to output
        cv::imshow("Live", frame);

        if(record)
            output << frame;

        // Spin until a frame duration has passed while checking user input, the high resolution click is used timing for accuracy
        std::chrono::time_point<std::chrono::high_resolution_clock> frameTime = std::chrono::high_resolution_clock::now();

        press = cv::waitKey(1);

        if(press == 'r')
        {
            std::cout << "Recording..." << std::endl;
            record = true;
        }

        while(true)
        {
            std::chrono::time_point<std::chrono::high_resolution_clock> sleepTime = std::chrono::high_resolution_clock::now();

            double framesElasped = std::chrono::duration_cast<frameDuration>(sleepTime - frameTime).count();

            if(framesElasped >= 1) break;
        }
    }

    // Stop the kinect device
    device->stopVideo();
    device->stopDepth();

    std::cout << "Done" << std::endl;

    return 0;
}
