#include "OpenCVKinectDevice.hpp"

OpenCVKinectDevice::OpenCVKinectDevice(freenect_context *_ctx, int _index)
    : Freenect::FreenectDevice(_ctx, _index), _Depth(cv::Size(640,480),CV_16U, cv::Scalar(0)), _Color(cv::Size(640,480),CV_8UC3, cv::Scalar(0))
{

}

void OpenCVKinectDevice::VideoCallback(void *rgb, uint32_t timestamp)
{
    _ColorLock.lock();

    cv::Mat rgbMat(cv::Size(640, 480), CV_8UC3, rgb);
    cv::cvtColor(rgbMat, _Color, CV_RGB2BGR);

    _ColorLock.unlock();
}

void OpenCVKinectDevice::DepthCallback(void *depth, uint32_t timestamp)
{
    _DepthLock.lock();

    cv::Mat depthImg16(cv::Size(640, 480), CV_16U, depth);
    depthImg16.copyTo(_Depth);

    _DepthLock.unlock();
}

void OpenCVKinectDevice::Color(cv::Mat &output)
{
    _ColorLock.lock();

    _Color.copyTo(output);

    _ColorLock.unlock();
}

void OpenCVKinectDevice::RawDepth(cv::Mat &output)
{
    _DepthLock.lock();

    _Depth.copyTo(output);

    _DepthLock.unlock();
}

void OpenCVKinectDevice::GrayDepth(cv::Mat &output)
{
    _DepthLock.lock();

    _Depth.convertTo(output, CV_8U, 255.0/2048.0); // Scale from 2048 max

    _DepthLock.unlock();
}

void OpenCVKinectDevice::WorldDepth(cv::Mat &output)
{
    _DepthLock.lock();

    output.create(_Depth.rows, _Depth.cols, CV_64FC3);

    const double k1 = 1.1863;
    const double k2 = 2842.5;
    const double k3 = 0.1236;

    const double minDist = -10;
    const double scale = 0.0021;

    for(int j = 0; j < _Depth.rows; j++)
    {
        for(int i = 0; i < _Depth.cols; i++)
        {
            unsigned short rawDepth = _Depth.at<unsigned short>(j, i);

            double z = k3 * std::tan(((double)rawDepth / k2) + k1);
            double x = ((double)i - ((double)_Depth.cols / 2.0)) * (z + minDist) * scale;
            double y = ((double)j - ((double)_Depth.rows / 2.0)) * (z + minDist) * scale;

            cv::Vec3d el;
            el[0] = x;
            el[1] = y;
            el[2] = z;

            output.at<cv::Vec3d>(j, i) = el;
        }
    }

    _DepthLock.unlock();
}

void OpenCVKinectDevice::Tilt(double angle)
{
    setTiltDegrees(angle);
}

double OpenCVKinectDevice::Tilt(void)
{
    updateState();

    double angle = getState().getTiltDegs();
    return angle;
}

void OpenCVKinectDevice::TiltUp(double dt)
{
    updateState();

    double angle = getState().getTiltDegs();
    setTiltDegrees(angle + dt);
}

void OpenCVKinectDevice::TiltDown(double dt)
{
    updateState();

    double angle = getState().getTiltDegs();
    setTiltDegrees(angle - dt);
}
