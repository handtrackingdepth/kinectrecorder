#include <libfreenect/libfreenect.hpp>
#include <opencv2/opencv.hpp>
#include <mutex>
#include <vector>

class OpenCVKinectDevice : public Freenect::FreenectDevice
{
  public:
    OpenCVKinectDevice(freenect_context *_ctx, int _index);

    void VideoCallback(void* _rgb, uint32_t timestamp);

    void DepthCallback(void* _depth, uint32_t timestamp);

    void TiltUp(double dt = 5);
    void TiltDown(double dt = 5);

    void Tilt(double angle);
    double Tilt(void);

    void Color(cv::Mat &output);

    void RawDepth(cv::Mat &output);
    void GrayDepth(cv::Mat &output);
    void WorldDepth(cv::Mat &output);

  private:
    cv::Mat _Depth;
    cv::Mat _Color;

    std::mutex _ColorLock;
    std::mutex _DepthLock;
};
