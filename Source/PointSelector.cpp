#include <opencv2/opencv.hpp>
#include <string>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <functional>

// Define some ascii keycode constants
const char ESCAPE = 0x1B;
const char BACKSPACE = 0x8;
const char SPACE = 0x20;

const cv::Scalar GREEN(0, 255, 0);

// Keeping these global so they can be accessed by opencv callbacks
int frameNumber  = 0;
std::vector<cv::Point2i> handPoints;

int main(int argc, char **argv)
{
    // Step 1: Setup

    //Get and open the video file
    if(argc < 1)
    {
        std::cout << "Usage: PointSelector [videoFileName]" << std::endl;
        return 0;
    }

    std::string videoFileName = argv[1];
    cv::VideoCapture vid(videoFileName);

    // Get the number of frames in the video and preallocate the points vector
    int maxFrame = vid.get(CV_CAP_PROP_FRAME_COUNT);
    std::cout << "Using " << videoFileName << " with " << maxFrame << " frames" << std::endl;
    handPoints.resize(maxFrame);

    // Crate a window with a mouse click callback to display the video and collect data
    cv::namedWindow("video", 1);
    cv::setMouseCallback("video", [](int event, int x, int y, int flags, void *userdata)
    {
        if(event == cv::EVENT_LBUTTONDOWN)
        {
            handPoints[frameNumber] = cv::Point2i(x, y);
            std::cout << frameNumber << ": (" << x << "," << y << ")" << std::endl;
        }
    });

    // Step 2: Collect data
    for(frameNumber = 0; frameNumber < maxFrame;)
    {
        // Set the current frame
        vid.set(CV_CAP_PROP_POS_FRAMES, frameNumber);

        // Grab the vido frame
        cv::Mat frame;
        vid >> frame;

        // If the frame was already clicked, draw the point for reference
        if(handPoints[frameNumber] != cv::Point2i())
            cv::circle(frame, handPoints[frameNumber], 1, GREEN);

        // Show the frame and let the user click it
        cv::imshow("video", frame);

        // Get user input
        char key = '\0';
        while(key != SPACE && key != BACKSPACE && key != ESCAPE)
            key = cv::waitKey(1);

        if(key == SPACE)    // Space -- forward one frame
            frameNumber = std::min(maxFrame, frameNumber + 1);
        else if(key == BACKSPACE) // Backspace -- back one frame
            frameNumber = std::max(0, frameNumber - 1);
        else if(key == ESCAPE) // Escape -- quit early
            break;

        std::cout << frameNumber << std::endl;
    }

    // Step 3: Write data to JSON file

    // Filter out any unclicked frames (considered to be points at (0,0), if you clicked a point at (0,0) then too bad for you)
    auto endrange = std::remove_if(handPoints.begin(), handPoints.end(), std::bind1st(std::equal_to<cv::Point2i>(), cv::Point2i()));
    handPoints.erase(endrange, handPoints.end());

    // Write the trajectory to a JSON file (uses an array of arrays notation)
    if(handPoints.size() > 0)
    {
        std::ofstream trajectoryFile("trajectory.json");
        trajectoryFile << "[" << std::endl;

        // Write all but the last point (to prevent a trailing comma)
        std::for_each(handPoints.begin(), handPoints.end() - 1, [&trajectoryFile](cv::Point2i pt)
        {
            trajectoryFile << "\t[ " << std::endl;
            trajectoryFile << "\t\t" << pt.x << "," << std::endl;
            trajectoryFile << "\t\t" << pt.y << std::endl;
            trajectoryFile << "\t], " << std::endl;
        });

        // Write the last click
        trajectoryFile << "\t[ " << std::endl;
        trajectoryFile << "\t\t" << (handPoints.end() - 1)->x << "," << std::endl;
        trajectoryFile << "\t\t" << (handPoints.end() - 1)->y << std::endl;
        trajectoryFile << "\t] " << std::endl;

        trajectoryFile << "]" << std::endl;
    }

    std::cout << "Done" << std::endl;
    return 0;
}
