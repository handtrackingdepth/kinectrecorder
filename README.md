KinectRecorder
==============

Record depth videos from the Kinect 1 sensor. Can save AVI grayscale depth or PPM 16U depth maps in image lists